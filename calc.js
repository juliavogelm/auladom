let counter = 0;
let lista_notas = [];

function guardar() {
    lista_notas.push(parseFloat(document.getElementById("valor").value));
    if (lista_notas[counter] === '') {
        alert("Por favor, insira uma nota");
    } else if (isNaN(lista_notas[counter])) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else if (parseFloat(lista_notas[counter]) < 0) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else if (parseFloat(lista_notas[counter]) > 10) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else {
        const div = document.querySelector('main');
        const mensagem = document.createElement('p');
        counter += 1;
        mensagem.textContent = "A nota " + counter + " foi " + lista_notas[counter - 1];
        div.appendChild(mensagem);
        console.log("Valor:", lista_notas[counter - 1]);
        document.getElementById("valor").value = '';
    }
}

function somarLista(lista) {
    return lista.reduce((acumulador, elemento) => acumulador + elemento, 0);
}

function calcmedia() {
    let resultado = somarLista(lista_notas) / counter;
    console.log("Soma:", resultado);
    let span = document.getElementById('resultado');
    span.textContent = resultado.toFixed(2); // Mostrar a média com 2 casas decimais
    counter = 0;
    lista_notas = [];
    console.log("Média:", resultado);
}





